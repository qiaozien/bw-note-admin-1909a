/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useRequest } from "ahooks"
import { getUserList } from "@/api/users"
import { ProTable, ProColumns, ActionType } from '@ant-design/pro-components';
import { RedoOutlined } from '@ant-design/icons';
import { Button } from "antd"
import { useRef } from "react"
import BaseSelect from "@/components/baseSelect"
const Users = () => {
  const actionRef = useRef<ActionType | undefined>();
  const { runAsync } = useRequest<any, any>(getUserList, {
    manual: true
  })  // 不用自己手动去写useEffect了
  const columns:ProColumns = [
    {
      title: '账号',
      dataIndex: 'name'
    },
    {
      title: '邮箱',
      dataIndex: 'email'
    },
    {
      title: '角色',
      dataIndex: 'role',
      renderFormItem: (item, { type, defaultRender, ...rest }) => {
        return <BaseSelect title="角色" {...rest} options={[
          {
            title: '访客',
            key: 'visitor'
          },
          {
            title: '管理员',
            key: 'admin'
          }
        ]}/>
      }
    },
    {
      title: '状态',
      dataIndex: 'status',
      renderFormItem: (item, { type, defaultRender, ...rest }) => {
        return <BaseSelect 
        {...rest}
        title="状态"
        options={[
          {
            title: '锁定',
            key: 'locked'
          },
          {
            title: '可用',
            key: 'active'
          }
        ]}/>
      }
    },
    {
      title: '注册日期',
      dataIndex: 'createAt',
      renderFormItem: () => null
    },
    {
      title: '操作',
      render:() => {
        return <div>
          <Button type="link">警用</Button>
        </div>
      },
      renderFormItem: () => null
    }
  ]
  return (
    <ProTable 
      rowSelection={{ // 多选
        type: 'checkbox'
      }}
      actionRef={actionRef}
      columns={columns} // 列配置
      rowKey={'id'}
      pagination={{ // 默认分页数据
        current: 1,
        pageSize: 10
      }}
      search={{
        searchText: '查询',
        span: 5,
        optionRender: (searchConfig,formProps,dom) => {
          return dom;
        }
      }}
      toolbar={{
        actions: [
          <Button
            key="reload"
            icon={<RedoOutlined />}
            onClick={() => {
              actionRef.current.reload && actionRef.current.reload();
            }}
          ></Button>,
        ],
        settings: []
      }}
      request={
        async (options) => { // 初始化创建表格执行 分页改变执行 点击查询的时候会执行
          console.log('request-------:',options);
          options = {
            ...options,
            page: options.current,
          }
          delete options.current;
          const { data } = await runAsync(options);  // 发请求
          return {
            data: data[0],
            success: true,
            total: data[1]
          }
        }
      }
    />
  )
}
export default Users;