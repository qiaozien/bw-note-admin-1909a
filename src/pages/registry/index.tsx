/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import {
  LoginFormPage,
  ProFormText,
} from '@ant-design/pro-components';
import { history } from "umi";
import style from "../login/login.less"
// import { useState } from 'react';
const Registry = () => {
  const hanldeClickRegistry = () => {
    //跳转注册页面
    history.push('/login')
  }
  const hanldeClickLogin = async (values:any) => {
    console.log(values)
    //发送请求
  }
  return (
    <div className={style.loginWrapper}>
      <LoginFormPage 
        backgroundImageUrl="https://www.bwie.com/static/home/images/welcome-hero/banner23.jpg"
        logo="https://www.bwie.com/static/home/logo/logo.jpg"
        title="八维创作平台"
        subTitle="CMS管理平台"
        layout="inline"
        onFinish={hanldeClickLogin}
        actions={
          <span onClick={hanldeClickRegistry}>
            已有账号去登录
          </span>
        }
      >
        <>
          <ProFormText
            name="name"
            labelAlign="left"
            placeholder={'请输入用户名'}
            label={'账号'}
            rules={[
              {
                required: true,
                message: '请输入用户名!',
              },
            ]}
          />
          <ProFormText.Password
            name="password"
            placeholder={'请输入密码'}
            label={'密码'}
            rules={[
              {
                required: true,
                message: '请输入密码！',
              },
            ]}
          />
        </>
      </LoginFormPage>
    </div>
  )
}

export default Registry;