/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import style from "./index.style.less"
import MDEditor, { commands } from '@uiw/react-md-editor';
import { useState, useRef, useEffect } from "react"
import { Button, Upload, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { upFile } from "@/api/files"
import _ from "lodash"
import content from "@/config/defaultArticleCont"
interface IMenuData {
  title: string;
  type: string;
  level: number;
  key: number
}

const CreateEditor = () => {
  const [value, setValue] = useState(() => content);
  const [menuData, setMenuData] = useState<IMenuData[]>([]);
  const perview = useRef<any>();
  const forMatterMenuData = () => {
    const previewEl = perview.current.querySelector('.w-md-editor-preview');
    const menuDataEl = Array.from(previewEl.querySelectorAll('*')).filter(item => /^H[1-6]$/.test(item.nodeName));
    setMenuData(menuDataEl.map<IMenuData>((item: any,i) => ({
      title: item.innerText,
      type: item.nodeName,
      level: item.nodeName.slice(1) * 1,
      key: i
    })))
  }
  useEffect(() => {
    // console.log('change-----', perview.current , value);
    setTimeout(() => { // 异步任务
      if(perview.current && value){
        // console.log('init----计算侧边蓝导航')
        forMatterMenuData(); // 重新计算侧边栏导航
      }
    }, 0)
  }, [perview, value])
  const hanleChange = (e) => {
    setValue(e);
  }
  const hanleUpload = async (file) => { // 自定义上传行为
    const Data = new FormData();
    Data.append('file', file.file) // 表单格式发送
    const { data } = await upFile(Data)
    message.success('上传成功');
    setValue((val) => 
      `${val}\n![${data.filename}](${data.url})`
    )
  }
  return (
    <div className={style['editor-wrapper']}>
      <div className={style['header']}>
        
      </div>
      <div className={style['editor']}>
        {/**编辑区域 */}
        <MDEditor
          value={value}
          ref={(val) => {
            if(val){
              perview.current = val.container;
            }
          }}
          onChange={_.throttle(hanleChange, 500)}
          commands={[  // 自定义toolbar
            {
              name:'test',
              keyCommand: 'test',
              icon: (
                <Upload
                  accept="image/png, image/jpeg, image/gif, image/jpg, image/svg"
                  customRequest={hanleUpload}
                  showUploadList={false}
                >
                  <Button 
                    icon={<UploadOutlined />} 
                    type="link"
                  ></Button>
                </Upload>
              )
            },
            commands.divider,
            commands.bold,
          ]}
        >
          <MDEditor.Markdown source={value} />
        </MDEditor>
        <div className={style.menu}>
          大纲
          {
            menuData.map(val => (
              <p key={val.key} style={{marginLeft: val.level * 5 }}>
                {val.title}
              </p>
            ))
          }
        </div>
      </div>
    </div>
  )
}

export default CreateEditor;