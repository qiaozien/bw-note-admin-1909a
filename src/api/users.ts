/*
 * @Author: Yh
 * @LastEditors: Yh
 */

import { request } from "umi"
import { BasePageParams } from "./api.d"

export const getUserList = (params:BasePageParams = {page:1, pageSize: 10}) => request('/api/user', {
  params
})