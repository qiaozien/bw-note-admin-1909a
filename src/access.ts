/*
 * @Author: Yh
 * @LastEditors: Yh
 * @info: 定义权限文件 文件名不能错
 * 
 * access 权限插件
 */

export default function (initState){   //被谁调用的 umijs调用的 调用的时候如何传参
  console.log('access',initState);
  return {
    isAdmin: initState?.role === 'admin'
  }
}