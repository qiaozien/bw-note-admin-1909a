/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import style from "./index.less"
import { EChartsType, init, EChartsOption, dispose } from "echarts"
import { useEffect, useRef } from "react"
type Types = 'pie' | 'bar' | 'line';

interface OptionType {
  type: Types;
  series: any[];
  xData?: any[];
  yData?: any[];
  chartData?: any;
}
interface Props extends OptionType{
  width?: number;
  height?: number;
  loading?: boolean;
}

const formatOption = ({type, series, xData, yData, chartData}: OptionType):EChartsOption => {
  switch(type){
    case 'pie': 
    return {
      ...chartData,
      series
    }
    case 'bar': 
    case 'line':
    return {
      ...chartData,
      xAxis: {
        type: 'category',
        data: xData
      },
      yAxis: {
        type: 'value'
      },
      series
    }
  }
}
const BaseChart = ({ 
  type = 'bar', 
  series = [],  // 图表主数据
  width = 300, 
  height = 150,
  xData = [], // x轴数据
  yData = [],  // y轴数据
  chartData = {},
  loading = true
}:Props = {type:'bar', series: [], width: 300, height: 150, xData: [], yData: [], chartData: {}, loading: true}) => {
  const chartInstance = useRef<null | EChartsType >(null);
  const chartDom = useRef<null | HTMLDivElement>(null);
  useEffect(() => {
    if(chartDom.current){
      chartInstance.current = init(chartDom.current, '', { // 组件创建的时候实例一次图标
        width,
        height
      });
    }
    return () => {
      chartInstance.current?.dispose();  // 图表和图表内组件卸载
      chartInstance.current && dispose(chartInstance.current);  // 卸载dom所有的方法
    }
  }, [])

  useEffect(() => {
    // 图表配置 和 图表实例发生变化
    if(chartInstance.current){
      chartInstance.current.setOption(formatOption({type,series,xData,yData,chartData}))
    }
  }, [type, series, xData, yData, chartInstance])

  useEffect(() => {  
    // 设置图表loading
    if(chartInstance.current){
      loading ? chartInstance.current.showLoading() : chartInstance.current.hideLoading()
    }
  },[loading, chartInstance])

  return (
    <div className={style.echartWrapper} ref={chartDom}></div>
  )
}

export default BaseChart;