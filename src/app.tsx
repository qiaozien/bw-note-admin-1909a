/*
 * @Author: Yh
 * @LastEditors: Yh
 * @info: 项目运行时配置，在浏览器中执行，可以获取到dom
 */
import "./app.less";
import { RequestConfig, history } from "umi"
import { localStorage } from "@/utils/storage"
import { BW_NOTE_USER_INFO } from "@/config/localStorageKey"
import { Button, Dropdown, Menu } from 'antd';


// 启用 initial-state插件
export const getInitialState = () => { // 该函数只在页面加载时执行一次
  try {
    const userInfo = localStorage.get(BW_NOTE_USER_INFO);  // 获取本地存储的用户信息
    if(!userInfo){
      history.replace('/login')
    }
    return userInfo;
  } catch (error) {
    console.log(error);
  }
}

export const layout = () => {
  const menu = (
    <Menu   // 虚拟dom  js对象 结构都是后期通过js动态渲染上的 给根元素绑定事件
      onClick={({key, domEvent}) => {
        domEvent.stopPropagation(); // 阻止事件传播
        history.push(key);
      }}  
      items={[
        {
          key: '/create/article',
          label: '新建文章'
        },
        {
          key: '/create/page',
          label: '新建页面'
        }
      ]}
    />
  )
  return {
    menuHeaderRender: (logo:any, title:any) => {
    return  <div
              id="customize_menu_header"
            >
              {logo}
              {title}
              <Dropdown overlay={menu} placement="bottomLeft">
                <Button>新建文章</Button>
              </Dropdown>
            </div>
    }
  }
}
/**
 * [request 请求统一配置]
 *  统一处理请求前公共参数，如携带用户身份标识，携带csrf-token
 *  统一处理请求后错误结果提示
 */
const TOKEN_TYPE = 'Bearer '
export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {
    adaptor: (resData) => {
      console.log('修改响应结果：adaptor：',resData)
      return {
        ...resData,
        errorMessage: resData.msg  // success为false时，弹框提示errorMessage信息
      }
    }
  },
  requestInterceptors: [ // 请求前拦截器 
    (url, options) => {
      console.log('接口请求之前执行，处理请求接口时携带的公共参数', options, url);
      const headers = {
        ...options.headers
      };
      if(!options.isAuthorization){ // 获取本地存储的token
        headers.authorization =  `${TOKEN_TYPE} ${localStorage.get(BW_NOTE_USER_INFO)?.token}`
      } 
      return {
        url: `${url}`, // 请求地址
        options: { 
          ...options, 
          interceptors: true,
          headers
        }, // 请求头 请求参数 请求方式
      } 
    }
  ],
  responseInterceptors: [ // 响应前拦截器
    async (response) => {
      console.log('响应前拦截器', );
      const res = await response.json();
      if(res.statusCode === 403){
        res.msg = '您暂无该接口权限';
      }
      if(res.statusCode === 401){
        history.replace('/login');
      }
      return res;
    }
  ],
};