/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import {
  LoginFormPage,
  ProFormText,
} from '@ant-design/pro-components';
import { history } from "umi";
import style from "./login.less"
import { useLocalStorageState } from "ahooks"
import { BW_NOTE_USER_INFO } from "@/config/localStorageKey"
import { LoginData, login } from "@/api/user"
import { useModel } from 'umi';
// import { useState } from 'react';
const Login = () => {
  const [, setUserInfo] = useLocalStorageState(BW_NOTE_USER_INFO, {
    defaultValue: {}
  });
  const { refresh } = useModel('@@initialState');
  const hanldeClickRegistry = () => {
    //跳转注册页面
    history.push('/registry')
  }
  const hanldeClickLogin = async (values:LoginData) => {
    //发送请求
    const { data } = await login(values);
    setUserInfo({  // 存储用户信息
      ...data
    })
    // 让app/getInitialState 的函数重新执行
    refresh();
    // 跳转页面
    history.push('/');
  }
  return (
    <div className={style.loginWrapper}>
      <LoginFormPage 
        backgroundImageUrl="https://www.bwie.com/static/home/images/welcome-hero/banner23.jpg"
        logo="https://www.bwie.com/static/home/logo/logo.jpg"
        title="八维创作平台"
        subTitle="CMS管理平台"
        layout="inline"
        onFinish={hanldeClickLogin}
        actions={
          <span onClick={hanldeClickRegistry}>
            or注册用户
          </span>
        }
      >
        <>
          <ProFormText
            name="name"
            labelAlign="left"
            placeholder={'请输入用户名'}
            label={'账号'}
            rules={[
              {
                required: true,
                message: '请输入用户名!',
              },
            ]}
          />
          <ProFormText.Password
            name="password"
            placeholder={'请输入密码'}
            label={'密码'}
            rules={[
              {
                required: true,
                message: '请输入密码！',
              },
            ]}
          />
        </>
      </LoginFormPage>
    </div>
  )
}

export default Login;