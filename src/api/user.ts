/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { request } from "umi"

export interface LoginData {
  name:string;
  password: string;
}

export const login = (data: LoginData) => request('/api/auth/login', { 
  method: 'POST',
  data,
  isAuthorization: true // 不需要携带用户身份标识
})