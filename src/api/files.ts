import { request } from "umi"

export const upFile = (body) => request('/api/file/upload',{
  method:'post',
  data: body
})