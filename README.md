<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
## ProComponents 

Ant Design 定义了基础的设计规范， 基础的ui组件库

Pro 系列组件提供快速高效大家高质量中后台应用的能力


## 前后端接口联调

1. 请求地址 url 发ajax请求必须要有请求地址
2. 请求方法 get、post、put、delete、update、option
3. 请求头 用户身份标识
4. 请求参数 get请求通过url地址进行拼接 post请求通过body传送


## ajax
1. 创建ajax对象
```js
const ajax = new XMLHttpRequest();
```
执行请求前拦截器，返回合法的参数（url,method,body,header），才会调用open方法链接服务器 
2. 链接服务器
```js
// 执行请求前拦截器
const {method, url, body} = requeset拦截器(config);
if(url){
  ajax.setHeader('headerName','headerValue')
  ajax.open(method, url, sync)
}
```
3. 发送请求
```js
ajax.send(body)
```
4. 接受响应结果
```js
ajax.onload = (data) => {
  // 执行响应前拦截器
  cosnt newData = response拦截器(data);
  // 在页面中对data做处理
  resolve(newData)
}
```


## 为什么对axios做二次封装，为什么对umi-requset做二次封装

1. 请求前添加公共参数
2. 对错误结果做统一处理，比如网络超时，错误提示框， 大量减少try catch监听接口错误


## 怎么区分环境，针对不同环境做配置 ？
1. 开发
2. 测试
3. 生产


## 怎么使用pro-layout渲染首页侧边栏导航？

## 封装一个echart图标的通用组件？ 考虑要接受哪些props 抛出哪些方法 


## 权限
用户 -》 身份 -》 接口和视图
- 侧边栏导航菜单展示
- 视图层面 访客身份不应该访问用户管理视图，当前用户身份没有该视图权限返回403页面或者跳转登录页面
- 接口层面 携带用户身份标识，后台根据身份标识查询数据，没有该权限返回403


## ahooks useRequest使用起来

## pro-component/pro-table组件

401 和 403的区别， 401是识别不了身份 403是识别身份但是没有权限


## 事件三要素
1. 事件类型（click，mouseover）
2. 事件源（触发事件的dom节点e.target， 绑定事件的dom节点e.currentTarget）
3. 事件处理程序 （绑定事件触发的函数）

## 事件流
1. 冒泡
2. 捕获


vw vh 单位的区别 浏览器窗口的宽度和高度 
px rem em 单位的区别 
px是绝对单位
rem相对根元素的fontSize进行计算 和 em相对父元素fontSize进行计算 是相对单位


## 答辩流程
1. ppt
  - 介绍项目背景（八维创作平台 c端）
    - 做了什么（文章，评论，交友，详情，分享优化阅读体验：响应式，主题颜色切换，图片预览，图片翻转等；付费阅读）
    - 是为什么要做 项目比其他竞品好在什么地方， 吹你项目亮点了，响应式项目兼容多端 pc、pad、手机，国际化，付费
  - 项目技术选型
    - 开发角度：
      - 为什么不使用原生开发？
        直接操作dom，成本比较高，会频繁造成页面的回流和重绘，而且跳转页面时会重新加载整个页面
      - react和vue区别：

        共同点：

          都采用虚拟dom的概念: 好处:减少真实dom的操作，只关注自己需要关注的属性和方法； 跨平台开发
          都采用diff算法（比较的两个js对象）优化虚拟dom渲染
          都采用了单项数据流（from data）的方式来管理数据状态

        不同点：

          写法不同：

            vue提供指令、模板语法、和一些固定的配置项比如 methods data等
            react使用jsx语法可以灵活使用js语句，完成方法封装，写法要比vue灵活

          数据驱动视图的方式:

            vue操作data数据，然后通过Object.definedProperty监听data数据变化更新视图
            react修改state状态会返回新的虚拟dom，优化diff算法直接更新视图

          受控组件管理：

            vue提供了v-model这个指令简化了受控组件操作
            react需要自己绑定value 监听change事件

          文档社区：

            vue是中国人开发的文档更友好
            react社区更活跃，周边生态更好

        为什么选择使用react：

          1. 保证项目进度，结合组内成员的技术栈选择使用react
          2. 结合项目背景，响应式、中英文切换，数据量比较大，涉及后续功能迭代，选择使用react
          3. react语法简洁，可以自定义封装方法。生态环境比较好可以实现xxx功能

  - 介绍项目功能模块
    - 项目基础建设类 团队协作 格式规范 代码提交规范
    - 首页 文章 详情 评论等等 开发类
      - 业务流程（用户操作流程）
      - 问题处理（数据丢失。网络状态不好。loading状态）
      - 优化 上拉加载 图片懒加载 搜索防抖节流 减少http请求的
  - 介绍任务分配
    - 谁负责什么模块
  - 单独讲解
    - 介绍你负责模块的功能
    - 然后总结开发过程中碰见的问题
    - 总结异常情况 列表没数据 数据过多 图片加载失败 过大



  1. 你负责什么项目，做了什么，用了什么，解决了什么问题（平常的时候保持好记录问题的习惯）


  ### eventLoop

```js
  console.log(1)  // 1 

  setTimeout(() => { // 异步 放到eventloop等待执行
    console.log(2)
  },10)
  setTimeout(() => { // 异步 放到eventloop等待执行
    console.log(8)
  },0)
 
  function a(){ // 作用域形成 作用域现在不运行
    console.log(7) 
  }
   
  new Promise((resolve,reject) => {  // 同步操作 一旦new Promise之后立马执行
    console.log('6') // 2
    resolve(3) // 一旦调用了resolve 就从 pedding 到 成功了
    reject('error') // 不会走失败逻辑 状态已经发生变化了
  })
  .then(res => {  // 异步 放到eventloop等待执行 调用resolve执行
    console.log(res)  // 5
  })
  .catch(error => { // 异步 调用reject
    console.log(error)
  })

  console.log(5)  // 3
  a() // 4

// 同步操作  1 6 5 7 到24行所有脚本解析完成 同步操作执行完成
// eventloop 执行 先找微任务 在找宏任务 then:3  定时器： 8 2

// 从上到下运行 

// 异步任务：
// (定时器 事件 ajax/fetch)浏览器触发  宏任务
// Promise(包裹异步操作的容器) es6新增的语法 微任务
```


## 项目性能优化

1. 代码层面 （写代码过程中需要注意的地方） 15%
  优化代码执行效率 
  公共方法的拆分, 请求公共状态的管理，axios的二次封装，对请求的错误处理，错误提示
  合理使用设计模式(写代码过程中一些套路) 单例 工厂 策略 发布订阅  代理（proxy）
  循环必须加key 在nodediff（节点diff）中key作为唯一标识方便进行比较，不推荐使用下标来当key
  不可变数据对象 对数据对象进行冻结 Object.freeze
  react：
    合理使用useCallback、useMemo、React.memo
    合理使用生命周期 shouldComponentUpdate

2. 资源管理 70% 所有的页面请求都属于资源
  静态资源（html\css\js\图片\视频\音频\iframe）一般不会发生改变
    减少静态资源文件加载时间
      1. 利用浏览器缓存
        - 强缓存 直接走浏览器缓存不会向服务器发送请求 0ms
        - 协商缓存 在强缓存失效的时候走协商缓存，服务器比较文件变化，
          文件没有变化：返回304状态码，不会返回文件资源 
          文件发生变化：返回200状态码，返回新的文件资源
      2. http传输压缩 发请求服务端响应的过程中压缩 
        - gzip压缩 http头：Content-Encoding:gzip 内容编码 可以压缩80%
      3. cdn（Content Delivery Network）: 分布式网络： 使用户就近获取所需内容，降低网络拥塞，提高用户访问响应速度和命中率
    提升项目首次打开速度
      1. 减少http请求
        图片懒加载（默认不给图片的src属性 当图片展示的时候替换src属性）
        上拉加载移动端分页 计算滚动距离，判断滚动到底部 滚走距离 + 可视区域距离 >= 页面高度
        分页
        搜索或者滚动条事件，频发触发的事件时候使用防抖节流，一定要搞清楚使用场景
        善用loading
        组件懒加载 import() 什么时候显示该组件什么时候加载
        合并js文件 可以吧常用的库合并到一个文件中
        雪碧图 合并css图片文件

    http状态码
      200-300之前都是成功 
      300-400之间都是缓存 304协商缓存也属于成功
      400-500客户端错误 先检查自己的参数是否正确 404（地址写错了） 401（用户身份不正确）403（用户身份不支持该权限）
      500以上就是服务端错误（直接找后端）
  接口(ajax\fetch) 实时更新的数据

3. 打包优化 15% webpack
  1. 配置别名 减少路径查找时间
  2. 区分环境 开发环境生产sourceMap 生产环境不需要定位源码 通过process.env来区分
  3. 打包压缩 js css 图片等资源文件 脚手架内置 (执行npm run build 就会走生产环境 会执行压缩)


### 项目上线

1. npm run build 打包生成js、css、html文件
2. 把文件从本地电脑上传到服务器指定位置
3. 配置生产环境服务器（nginx软件，解决history404问题和反向代理）
4. 重启生产环境服务器